const express = require("express");

const {
  getTask,
  updateTaskComplete,
} = require("../controllers/tasksController");

const router = express.Router();

router.get("/:id", getTask);

router.put("/:id/complete", updateTaskComplete);

module.exports = router;
