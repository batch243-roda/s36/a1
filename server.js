const express = require("express");
const mongoose = require("mongoose");

const tasksRoutes = require("./routes/tasksRoutes");

const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/tasks", tasksRoutes);

mongoose
  .connect(
    "mongodb+srv://jroda:admin@zuittbatch243-roda.4axrb9t.mongodb.net/B243-to-do?retryWrites=true&w=majority"
  )
  .then(() => {
    app.listen(port, () => console.log(`Server is running at port ${port}`));
  })
  .catch((err) => {
    console.log(err);
  });
