const mongoose = require("mongoose");

const Task = require("../models/taskModel");

const getTask = async (req, res) => {
  const { id } = req.params;

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(404).send({ error: "No such task" });
  }

  const task = await Task.findById(id);

  if (!task) {
    return res.status(404).send({ error: "No such task" });
  }

  res.status(200).send(task);
};

// Update specific task
const updateTaskComplete = async (req, res) => {
  const { id } = req.params;

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(404).send({ error: "No such task" });
  }

  const task = await Task.findByIdAndUpdate(
    { _id: id },
    {
      status: "complete",
    }
  );

  if (!task) {
    return res.status(404).send({ error: "No such task" });
  }

  res.status(200).send(task);
};

module.exports = { getTask, updateTaskComplete };
